#include "TimeStepController.h"
#include "Simulation/TimeManager.h"
#include "PositionBasedDynamics/PositionBasedRigidBodyDynamics.h"
#include "PositionBasedDynamics/TimeIntegration.h"
#include <iostream>
#include "PositionBasedDynamics/PositionBasedDynamics.h"
#include "Utils/Timing.h"

using namespace PBD;
using namespace std;
using namespace GenParam;

// int TimeStepController::SOLVER_ITERATIONS = -1;
// int TimeStepController::SOLVER_ITERATIONS_V = -1;
int TimeStepController::MAX_ITERATIONS = -1;
int TimeStepController::MAX_ITERATIONS_V = -1;
int TimeStepController::VELOCITY_UPDATE_METHOD = -1;
int TimeStepController::ENUM_VUPDATE_FIRST_ORDER = -1;
int TimeStepController::ENUM_VUPDATE_SECOND_ORDER = -1;


TimeStepController::TimeStepController() 
{
	m_velocityUpdateMethod = 0;
	m_iterations = 0;
	m_iterationsV = 0;
	m_maxIterations = 5;
	m_maxIterationsV = 5;
	m_collisionDetection = NULL;	
}

TimeStepController::~TimeStepController(void)
{
}

void TimeStepController::initParameters()
{
	TimeStep::initParameters();

// 	SOLVER_ITERATIONS = createNumericParameter("iterations", "Iterations", &m_iterations);
// 	setGroup(SOLVER_ITERATIONS, "PBD");
// 	setDescription(SOLVER_ITERATIONS, "Iterations required by the solver.");
// 	getParameter(SOLVER_ITERATIONS)->setReadOnly(true);

	MAX_ITERATIONS = createNumericParameter("maxIterations", "Max. iterations", &m_maxIterations);
	setGroup(MAX_ITERATIONS, "PBD");
	setDescription(MAX_ITERATIONS, "Maximal number of iterations of the solver.");
	static_cast<NumericParameter<unsigned int>*>(getParameter(MAX_ITERATIONS))->setMinValue(1);

// 	SOLVER_ITERATIONS_V = createNumericParameter("iterationsV", "Velocity iterations", &m_iterationsV);
// 	setGroup(SOLVER_ITERATIONS_V, "PBD");
// 	setDescription(SOLVER_ITERATIONS_V, "Iterations required by the velocity solver.");
// 	getParameter(SOLVER_ITERATIONS_V)->setReadOnly(true);

	MAX_ITERATIONS_V = createNumericParameter("maxIterationsV", "Max. velocity iterations", &m_maxIterationsV);
	setGroup(MAX_ITERATIONS_V, "PBD");
	setDescription(MAX_ITERATIONS_V, "Maximal number of iterations of the velocity solver.");
	static_cast<NumericParameter<unsigned int>*>(getParameter(MAX_ITERATIONS_V))->setMinValue(0);

	VELOCITY_UPDATE_METHOD = createEnumParameter("velocityUpdateMethod", "Velocity update method", &m_velocityUpdateMethod);
	setGroup(VELOCITY_UPDATE_METHOD, "PBD");
	setDescription(VELOCITY_UPDATE_METHOD, "Velocity method.");
	EnumParameter* enumParam = static_cast<EnumParameter*>(getParameter(VELOCITY_UPDATE_METHOD));
	enumParam->addEnumValue("First Order Update", ENUM_VUPDATE_FIRST_ORDER);
	enumParam->addEnumValue("Second Order Update", ENUM_VUPDATE_SECOND_ORDER);
}

void TimeStepController::step(SimulationModel &model)
{
	START_TIMING("simulation step");
	TimeManager *tm = TimeManager::getCurrent ();
	const Real h = tm->getTimeStepSize();
        std::cout<<"time step size = "<<h<<std::endl;
 
	//////////////////////////////////////////////////////////////////////////
	// rigid body model
	//////////////////////////////////////////////////////////////////////////
	clearAccelerations(model);
	SimulationModel::RigidBodyVector &rb = model.getRigidBodies();
	ParticleData &pd = model.getParticles();
	OrientationData &od = model.getOrientations();

	const int numBodies = (int)rb.size();
	#pragma omp parallel if(numBodies > MIN_PARALLEL_SIZE) default(shared)
	{
		#pragma omp for schedule(static) nowait
		for (int i = 0; i < numBodies; i++)
		{ 
			rb[i]->getLastPosition() = rb[i]->getOldPosition();
			rb[i]->getOldPosition() = rb[i]->getPosition();
			TimeIntegration::semiImplicitEuler(h, rb[i]->getMass(), rb[i]->getPosition(), rb[i]->getVelocity(), rb[i]->getAcceleration());
			rb[i]->getLastRotation() = rb[i]->getOldRotation();
			rb[i]->getOldRotation() = rb[i]->getRotation();
			TimeIntegration::semiImplicitEulerRotation(h, rb[i]->getMass(), rb[i]->getInertiaTensorInverseW(), rb[i]->getRotation(), rb[i]->getAngularVelocity(), rb[i]->getTorque());
			rb[i]->rotationUpdated();
		}
	}

	START_TIMING("position constraints projection");
	// positionConstraintProjection(model);
	STOP_TIMING_AVG;

	#pragma omp parallel if(numBodies > MIN_PARALLEL_SIZE) default(shared)
	{
		// Update velocities	
		#pragma omp for schedule(static) nowait
		for (int i = 0; i < numBodies; i++)
		{
                        // MARKER
			if (m_velocityUpdateMethod == 0)
			{
				TimeIntegration::velocityUpdateFirstOrder(h, rb[i]->getMass(), rb[i]->getPosition(), rb[i]->getOldPosition(), rb[i]->getVelocity());
				TimeIntegration::angularVelocityUpdateFirstOrder(h, rb[i]->getMass(), rb[i]->getRotation(), rb[i]->getOldRotation(), rb[i]->getAngularVelocity());
			}
			else
			{
				TimeIntegration::velocityUpdateSecondOrder(h, rb[i]->getMass(), rb[i]->getPosition(), rb[i]->getOldPosition(), rb[i]->getLastPosition(), rb[i]->getVelocity());
				TimeIntegration::angularVelocityUpdateSecondOrder(h, rb[i]->getMass(), rb[i]->getRotation(), rb[i]->getOldRotation(), rb[i]->getLastRotation(), rb[i]->getAngularVelocity());
			}
			// update geometry
			if (rb[i]->getMass() != 0.0)
				rb[i]->getGeometry().updateMeshTransformation(rb[i]->getPosition(), rb[i]->getRotationMatrix());
		}
	}

	if (m_collisionDetection)
	{
		START_TIMING("collision detection");
		m_collisionDetection->collisionDetection(model);
		STOP_TIMING_AVG;
	}

	velocityConstraintProjection(model);


	// compute new time	
	tm->setTime (tm->getTime () + h);

        std::cout<<"===== report at the end of t = "<<(tm->getTime())<<std::endl;
	for (int i = 0; i < numBodies; i++)
	{ 
          std::cout<<"[body "<<i<<"]"<<std::endl;
          std::cout<<"  x = "<<rb[i]->getPosition()[0]<<" "<<rb[i]->getPosition()[1]<<" "<<rb[i]->getPosition()[2]<<std::endl;
          std::cout<<"  lastx = "<<rb[i]->getLastPosition()[0]<<" "<<rb[i]->getLastPosition()[1]<<" "<<rb[i]->getLastPosition()[2]<<std::endl;
          std::cout<<"  oldx = "<<rb[i]->getOldPosition()[0]<<" "<<rb[i]->getOldPosition()[1]<<" "<<rb[i]->getOldPosition()[2]<<std::endl;

          std::cout<<"  v = "<<rb[i]->getVelocity()[0]<<" "<<rb[i]->getVelocity()[1]<<" "<<rb[i]->getVelocity()[2]<<std::endl;
          std::cout<<"  a = "<<rb[i]->getAcceleration()[0]<<" "<<rb[i]->getAcceleration()[1]<<" "<<rb[i]->getAcceleration()[2]<<std::endl;

          std::cout<<"  q = "<<rb[i]->getRotation().w()<<" "<<rb[i]->getRotation().x()<<" "<<rb[i]->getRotation().y()<<" "<<rb[i]->getRotation().z()<<std::endl;
          std::cout<<"  lastq = "<<rb[i]->getLastRotation().w()<<" "<<rb[i]->getLastRotation().x()<<" "<<rb[i]->getLastRotation().y()<<" "<<rb[i]->getLastRotation().z()<<std::endl;
          std::cout<<"  oldq = "<<rb[i]->getOldRotation().w()<<" "<<rb[i]->getOldRotation().x()<<" "<<rb[i]->getOldRotation().y()<<" "<<rb[i]->getOldRotation().z()<<std::endl;
          std::cout<<"  omega = "<<rb[i]->getAngularVelocity()[0]<<" "<<rb[i]->getAngularVelocity()[1]<<" "<<rb[i]->getAngularVelocity()[2]<<std::endl;
          std::cout<<"  torque = "<<rb[i]->getTorque()[0]<<" "<<rb[i]->getTorque()[1]<<" "<<rb[i]->getTorque()[2]<<std::endl;
	}

	STOP_TIMING_AVG;
}

void TimeStepController::reset()
{
	m_iterations = 0;
	m_iterationsV = 0;
	m_maxIterations = 5;
	m_maxIterationsV = 5;
}

void TimeStepController::positionConstraintProjection(SimulationModel &model)
{
	m_iterations = 0;

	// init constraint groups if necessary
	model.initConstraintGroups();

	SimulationModel::RigidBodyContactConstraintVector &contacts = model.getRigidBodyContactConstraints();

	// init constraints for this time step if necessary
        std::cout<<"contacts.size = "<<contacts.size()<<std::endl;

	while (m_iterations < m_maxIterations)
	{
		m_iterations++;
	}
}


void TimeStepController::velocityConstraintProjection(SimulationModel &model)
{
	m_iterationsV = 0;

	// init constraint groups if necessary
	model.initConstraintGroups();

	SimulationModel::RigidBodyContactConstraintVector &rigidBodyContacts = model.getRigidBodyContactConstraints();

        std::cout<<"m_maxIterationsV = "<<m_maxIterationsV<<std::endl;
	while (m_iterationsV < m_maxIterationsV)
	{
          std::cout<<"iteration = "<<m_iterationsV<<std::endl;
		// solve contacts
		for (unsigned int i = 0; i < rigidBodyContacts.size(); i++)
		{
                  // m_constraintInfo is not modified
			rigidBodyContacts[i].solveVelocityConstraint(model, m_iterationsV);
		}
		m_iterationsV++;
	}
}


