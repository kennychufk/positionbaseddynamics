#ifndef _CONSTRAINTS_H
#define _CONSTRAINTS_H

#include "Common/Common.h"
#define _USE_MATH_DEFINES
#include "math.h"
#include <vector>
#include <list>
#include <memory>

namespace PBD
{
	class SimulationModel;

	class Constraint
	{
	public: 
		unsigned int m_numberOfBodies;
		/** indices of the linked bodies */
		unsigned int *m_bodies;

		Constraint(const unsigned int numberOfBodies) 
		{
			m_numberOfBodies = numberOfBodies; 
			m_bodies = new unsigned int[numberOfBodies]; 
		}

		virtual ~Constraint() { delete[] m_bodies; };
		virtual int &getTypeId() const = 0;

		virtual bool initConstraintBeforeProjection(SimulationModel &model) { return true; };
		virtual bool updateConstraint(SimulationModel &model) { return true; };
		virtual bool solvePositionConstraint(SimulationModel &model, const unsigned int iter) { return true; };
		virtual bool solveVelocityConstraint(SimulationModel &model, const unsigned int iter) { return true; };
	};

	class RigidBodyContactConstraint 
	{
	public:
		static int TYPE_ID;
		/** indices of the linked bodies */
		unsigned int m_bodies[2];
		Real m_stiffness; 
		Real m_frictionCoeff;
		Real m_sum_impulses;
		Eigen::Matrix<Real, 3, 5, Eigen::DontAlign> m_constraintInfo;

		RigidBodyContactConstraint() {}
		~RigidBodyContactConstraint() {}
		virtual int &getTypeId() const { return TYPE_ID; }

		bool initConstraint(SimulationModel &model, const unsigned int rbIndex1, const unsigned int rbIndex2, 
			const Vector3r &cp1, const Vector3r &cp2, 
			const Vector3r &normal, const Real dist, 
			const Real restitutionCoeff, const Real stiffness, const Real frictionCoeff);
		virtual bool solveVelocityConstraint(SimulationModel &model, const unsigned int iter);
	};

}

#endif
