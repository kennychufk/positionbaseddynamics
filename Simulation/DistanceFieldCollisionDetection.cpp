#include "DistanceFieldCollisionDetection.h"
#include "Simulation/IDFactory.h"
#include "omp.h"

using namespace PBD;
using namespace Utilities;

int DistanceFieldCollisionDetection::DistanceFieldCollisionBox::TYPE_ID = IDFactory::getId();
int DistanceFieldCollisionDetection::DistanceFieldCollisionSphere::TYPE_ID = IDFactory::getId();
int DistanceFieldCollisionDetection::DistanceFieldCollisionTorus::TYPE_ID = IDFactory::getId();
int DistanceFieldCollisionDetection::DistanceFieldCollisionCylinder::TYPE_ID = IDFactory::getId();
int DistanceFieldCollisionDetection::DistanceFieldCollisionHollowSphere::TYPE_ID = IDFactory::getId();
int DistanceFieldCollisionDetection::DistanceFieldCollisionHollowBox::TYPE_ID = IDFactory::getId();
int DistanceFieldCollisionDetection::DistanceFieldCollisionObjectWithoutGeometry::TYPE_ID = IDFactory::getId();


DistanceFieldCollisionDetection::DistanceFieldCollisionDetection() :
	CollisionDetection()
{
}

DistanceFieldCollisionDetection::~DistanceFieldCollisionDetection()
{
}

void DistanceFieldCollisionDetection::collisionDetection(SimulationModel &model)
{
	model.resetContacts();
	const SimulationModel::RigidBodyVector &rigidBodies = model.getRigidBodies();
	const SimulationModel::TriangleModelVector &triModels = model.getTriangleModels();
	const SimulationModel::TetModelVector &tetModels = model.getTetModels();
	const ParticleData &pd = model.getParticles();

	std::vector < std::pair<unsigned int, unsigned int>> coPairs;
	for (unsigned int i = 0; i < m_collisionObjects.size(); i++)
	{
		CollisionDetection::CollisionObject *co1 = m_collisionObjects[i];
		for (unsigned int k = 0; k < m_collisionObjects.size(); k++)
		{
			CollisionDetection::CollisionObject *co2 = m_collisionObjects[k];
			if ((i != k))
			{
				// ToDo: self collisions for deformables
				coPairs.push_back({ i, k });
			}
		}
	}

	//omp_set_num_threads(1);
	std::vector<std::vector<ContactData> > contacts_mt;	
// #ifdef _DEBUG
	const unsigned int maxThreads = 1;
// #else
// 	const unsigned int maxThreads = omp_get_max_threads();
// #endif
	contacts_mt.resize(maxThreads);

	// #pragma omp parallel default(shared)
	{
		// Update BVHs
		// #pragma omp for schedule(static)  
		for (int i = 0; i < (int)m_collisionObjects.size(); i++)
		{
			CollisionDetection::CollisionObject *co = m_collisionObjects[i];
			updateAABB(model, co);
		}

		// #pragma omp for schedule(static)
		for (int i = 0; i < (int)coPairs.size(); i++)
		{
			std::pair<unsigned int, unsigned int> &coPair = coPairs[i];
			CollisionDetection::CollisionObject *co1 = m_collisionObjects[coPair.first];
			CollisionDetection::CollisionObject *co2 = m_collisionObjects[coPair.second];

			if ((co1->m_bodyType == CollisionDetection::CollisionObject::RigidBodyCollisionObjectType) &&
				(co2->m_bodyType == CollisionDetection::CollisionObject::RigidBodyCollisionObjectType) &&
				((DistanceFieldCollisionObject*) co1)->m_testMesh)
			{
				RigidBody *rb1 = rigidBodies[co1->m_bodyIndex];				
				RigidBody *rb2 = rigidBodies[co2->m_bodyIndex];
				const Real restitutionCoeff = rb1->getRestitutionCoeff() * rb2->getRestitutionCoeff();
				const Real frictionCoeff = rb1->getFrictionCoeff() + rb2->getFrictionCoeff();
				collisionDetectionRigidBodies(rb1, (DistanceFieldCollisionObject*)co1, rb2, (DistanceFieldCollisionObject*)co2,
					restitutionCoeff, frictionCoeff
					, contacts_mt
					);
			}
		}
	}

	m_tempContacts.clear();
        std::cout<<"add rigid body contacts"<<std::endl;
	for (unsigned int i = 0; i < contacts_mt.size(); i++)
	{
		for (unsigned int j = 0; j < contacts_mt[i].size(); j++)
		{
			if (contacts_mt[i][j].m_type == 0)
				addRigidBodyContact(contacts_mt[i][j].m_index1, contacts_mt[i][j].m_index2,
					contacts_mt[i][j].m_cp1, contacts_mt[i][j].m_cp2, contacts_mt[i][j].m_normal,
					contacts_mt[i][j].m_dist, contacts_mt[i][j].m_restitution, contacts_mt[i][j].m_friction);
		}
	}
}

void DistanceFieldCollisionDetection::collisionDetectionRigidBodies(RigidBody *rb1, DistanceFieldCollisionObject *co1, RigidBody *rb2, DistanceFieldCollisionObject *co2, 
	const Real restitutionCoeff, const Real frictionCoeff
	, std::vector<std::vector<ContactData> > &contacts_mt
	)
{
	if ((rb1->getMass() == 0.0) && (rb2->getMass() == 0.0))
		return;

	const VertexData &vd = rb1->getGeometry().getVertexData();
	const Vector3r &vertex_wfirst = vd.getPosition(0);
        const Quaternionr& rb1quat =rb1->getRotation();
        const Vector3r& rb1x =rb1->getPosition();
	const Vector3r &vertex_lfirst = rb1->getGeometry().getVertexDataLocal().getPosition(0);
        std::cout<<"body "<<co1->m_bodyIndex<<" x_l = "<<vertex_lfirst[0]<<" "<<vertex_lfirst[1]<<" "<<vertex_lfirst[2]<<" x_w = "<<vertex_wfirst[0]<<" "<<vertex_wfirst[1]<<" "<<vertex_wfirst[2]<<" q = "<< rb1quat.w()<<" "<< rb1quat.x()<<" "<< rb1quat.y()<<" "<< rb1quat.z()<<" pos = "<< rb1x[0]<<" "<< rb1x[1]<<" "<<rb1x[2]  <<std::endl;

	const Vector3r &com2 = rb2->getPosition();

	// remove the rotation of the main axis transformation that is performed
	// to get a diagonal inertia tensor since the distance function is 
	// evaluated in local coordinates
	//
	// transformation world to local:
	// p_local = R_initial^T ( R_MAT R^T (p_world - x) - x_initial + x_MAT)
	// 
	// transformation local to:
	// p_world = R R_MAT^T (R_initial p_local + x_initial - x_MAT) + x
	//
	const Matrix3r &R = rb2->getTransformationR();
	const Vector3r &v1 = rb2->getTransformationV1();
	const Vector3r &v2 = rb2->getTransformationV2();

	const PointCloudBSH &bvh = ((DistanceFieldCollisionDetection::DistanceFieldCollisionObject*) co1)->m_bvh;
	std::function<bool(unsigned int, unsigned int)> predicate = [&](unsigned int node_index, unsigned int depth)
	{
		const BoundingSphere &bs = bvh.hull(node_index);
		const Vector3r &sphere_x = bs.x();
		const Vector3r sphere_x_w = rb1->getRotation() * sphere_x + rb1->getPosition();

		AlignedBox3r box3f;
		box3f.extend(co2->m_aabb.m_p[0]);
		box3f.extend(co2->m_aabb.m_p[1]);
		const Real dist = box3f.exteriorDistance(sphere_x_w);

		// Test if center of bounding sphere intersects AABB
		if (dist < bs.r())
		{
			// Test if distance of center of bounding sphere to collision object is smaller than the radius
			const Vector3r x = R * (sphere_x_w - com2) + v1;
			const double dist2 = co2->distance(x.template cast<double>(), m_tolerance);
			if (dist2 == std::numeric_limits<double>::max())
				return true;
			if (dist2 < bs.r())
				return true;
		}
		return false;
	};
	std::function<void(unsigned int, unsigned int)> cb = [&](unsigned int node_index, unsigned int depth)
	{
		auto const& node = bvh.node(node_index);
		if (!node.is_leaf())
			return;

		for (auto i = node.begin; i < node.begin + node.n; ++i)
		{
			unsigned int index = bvh.entity(i);
			const Vector3r &x_w = vd.getPosition(index);
			const Vector3r x = R * (x_w - com2) + v1;
			Vector3r cp, n;
			Real dist;
			if (co2->collisionTest(x, m_tolerance, cp, n, dist))
			{
				const Vector3r cp_w = R.transpose() * cp + v2;
				const Vector3r n_w = R.transpose() * n;
                                std::cout<<co1->m_bodyIndex<<","<<co2->m_bodyIndex<<" x_i_k: ("<<x[0]<<" "<<x[1]<<" "<<x[2]<<") cp: ("<<cp[0]<<" "<<cp[1]<<" "<<cp[2]<<") cp_w_i: ("<<x_w[0]<<" "<<x_w[1]<<" "<<x_w[2]<<") cp_w_k: ("<<cp_w[0]<<" "<<cp_w[1]<<" "<<cp_w[2]<<") n = ("<<n[0]<<" "<<n[1]<<" "<<n[2]<<") n_w = ("<<n_w[0]<<" "<<n_w[1]<<" "<<n_w[2]<<") dist = "<<dist<<std::endl;

// #ifdef _DEBUG
				int tid = 0;
// #else
// 				int tid = omp_get_thread_num();
// #endif			

				contacts_mt[tid].push_back({ 0, co1->m_bodyIndex, co2->m_bodyIndex, x_w, cp_w, n_w, dist, restitutionCoeff, frictionCoeff });
			}
		}
	};
	bvh.traverse_depth_first(predicate, cb);
}

bool DistanceFieldCollisionDetection::isDistanceFieldCollisionObject(CollisionObject *co) const
{
	return (co->getTypeId() == DistanceFieldCollisionDetection::DistanceFieldCollisionBox::TYPE_ID) ||
		(co->getTypeId() == DistanceFieldCollisionDetection::DistanceFieldCollisionSphere::TYPE_ID) ||
		(co->getTypeId() == DistanceFieldCollisionDetection::DistanceFieldCollisionTorus::TYPE_ID) ||
		(co->getTypeId() == DistanceFieldCollisionDetection::DistanceFieldCollisionCylinder::TYPE_ID) ||
		(co->getTypeId() == DistanceFieldCollisionDetection::DistanceFieldCollisionHollowSphere::TYPE_ID) ||
		(co->getTypeId() == DistanceFieldCollisionDetection::DistanceFieldCollisionHollowBox::TYPE_ID) ||
		(co->getTypeId() == DistanceFieldCollisionDetection::DistanceFieldCollisionObjectWithoutGeometry::TYPE_ID);
}

void DistanceFieldCollisionDetection::addCollisionBox(const unsigned int bodyIndex, const unsigned int bodyType, const Vector3r *vertices, const unsigned int numVertices, const Vector3r &box, const bool testMesh, const bool invertSDF)
{
	DistanceFieldCollisionDetection::DistanceFieldCollisionBox *cf = new DistanceFieldCollisionDetection::DistanceFieldCollisionBox();
	cf->m_bodyIndex = bodyIndex;
	cf->m_bodyType = bodyType;
	// distance function requires 0.5*box 
	cf->m_box = 0.5*box;
	cf->m_bvh.init(vertices, numVertices);
	cf->m_bvh.construct();
	cf->m_testMesh = testMesh;
	if (invertSDF)
		cf->m_invertSDF = -1.0;
	m_collisionObjects.push_back(cf);
}

void DistanceFieldCollisionDetection::addCollisionSphere(const unsigned int bodyIndex, const unsigned int bodyType, const Vector3r *vertices, const unsigned int numVertices, const Real radius, const bool testMesh, const bool invertSDF)
{
	DistanceFieldCollisionDetection::DistanceFieldCollisionSphere *cs = new DistanceFieldCollisionDetection::DistanceFieldCollisionSphere();
	cs->m_bodyIndex = bodyIndex;
	cs->m_bodyType = bodyType;
	cs->m_radius = radius;
	cs->m_bvh.init(vertices, numVertices);
	cs->m_bvh.construct();
	cs->m_testMesh = testMesh;
	if (invertSDF)
		cs->m_invertSDF = -1.0;
	m_collisionObjects.push_back(cs);
}

void DistanceFieldCollisionDetection::addCollisionTorus(const unsigned int bodyIndex, const unsigned int bodyType, const Vector3r *vertices, const unsigned int numVertices, const Vector2r &radii, const bool testMesh, const bool invertSDF)
{
	DistanceFieldCollisionDetection::DistanceFieldCollisionTorus *ct = new DistanceFieldCollisionDetection::DistanceFieldCollisionTorus();
	ct->m_bodyIndex = bodyIndex;
	ct->m_bodyType = bodyType;
	ct->m_radii = radii;
	ct->m_bvh.init(vertices, numVertices);
	ct->m_bvh.construct();
	ct->m_testMesh = testMesh;
	if (invertSDF)
		ct->m_invertSDF = -1.0;
	m_collisionObjects.push_back(ct);
}

void DistanceFieldCollisionDetection::addCollisionCylinder(const unsigned int bodyIndex, const unsigned int bodyType, const Vector3r *vertices, const unsigned int numVertices, const Vector2r &dim, const bool testMesh, const bool invertSDF)
{
	DistanceFieldCollisionDetection::DistanceFieldCollisionCylinder *ct = new DistanceFieldCollisionDetection::DistanceFieldCollisionCylinder();
	ct->m_bodyIndex = bodyIndex;
	ct->m_bodyType = bodyType;
	ct->m_dim = dim;
	// distance function uses height/2
	ct->m_dim[1] *= 0.5;
	ct->m_bvh.init(vertices, numVertices);
	ct->m_bvh.construct();
	ct->m_testMesh = testMesh;
	if (invertSDF)
		ct->m_invertSDF = -1.0;
	m_collisionObjects.push_back(ct);
}

void DistanceFieldCollisionDetection::addCollisionHollowSphere(const unsigned int bodyIndex, const unsigned int bodyType, const Vector3r *vertices, const unsigned int numVertices, const Real radius, const Real thickness, const bool testMesh, const bool invertSDF)
{
	DistanceFieldCollisionDetection::DistanceFieldCollisionHollowSphere *cs = new DistanceFieldCollisionDetection::DistanceFieldCollisionHollowSphere();
	cs->m_bodyIndex = bodyIndex;
	cs->m_bodyType = bodyType;
	cs->m_radius = radius;
	cs->m_thickness = thickness;
	cs->m_bvh.init(vertices, numVertices);
	cs->m_bvh.construct();
	cs->m_testMesh = testMesh;
	if (invertSDF)
		cs->m_invertSDF = -1.0;
	m_collisionObjects.push_back(cs);
}

void DistanceFieldCollisionDetection::addCollisionHollowBox(const unsigned int bodyIndex, const unsigned int bodyType, const Vector3r *vertices, const unsigned int numVertices, const Vector3r &box, const Real thickness, const bool testMesh, const bool invertSDF)
{
	DistanceFieldCollisionDetection::DistanceFieldCollisionHollowBox *cf = new DistanceFieldCollisionDetection::DistanceFieldCollisionHollowBox();
	cf->m_bodyIndex = bodyIndex;
	cf->m_bodyType = bodyType;
	// distance function requires 0.5*box 
	cf->m_box = 0.5*box;
	cf->m_thickness = thickness;
	cf->m_bvh.init(vertices, numVertices);
	cf->m_bvh.construct();
	cf->m_testMesh = testMesh;
	if (invertSDF)
		cf->m_invertSDF = -1.0;
	m_collisionObjects.push_back(cf);
}

void DistanceFieldCollisionDetection::addCollisionObjectWithoutGeometry(const unsigned int bodyIndex, const unsigned int bodyType, const Vector3r *vertices, const unsigned int numVertices, const bool testMesh)
{
	DistanceFieldCollisionObjectWithoutGeometry *co = new DistanceFieldCollisionObjectWithoutGeometry();
	co->m_bodyIndex = bodyIndex;
	co->m_bodyType = bodyType;
	co->m_bvh.init(vertices, numVertices);
	co->m_bvh.construct();
	co->m_testMesh = testMesh;
	co->m_invertSDF = 1.0;
	m_collisionObjects.push_back(co);
}

double DistanceFieldCollisionDetection::DistanceFieldCollisionBox::distance(const Eigen::Vector3d &x, const Real tolerance)
{
	const Eigen::Vector3d box_d = m_box.template cast<double>();
	const Eigen::Vector3d x_d = x.template cast<double>();
	const Eigen::Vector3d d(fabs(x_d.x()) - box_d.x(), fabs(x_d.y()) - box_d.y(), fabs(x_d.z()) - box_d.z());
	const Eigen::Vector3d max_d(std::max(d.x(), 0.0), std::max(d.y(), 0.0), std::max(d.z(), 0.0));
	return m_invertSDF*(std::min(std::max(d.x(), std::max(d.y(), d.z())), 0.0) + max_d.norm()) - static_cast<double>(tolerance);
}

double DistanceFieldCollisionDetection::DistanceFieldCollisionSphere::distance(const Eigen::Vector3d &x, const Real tolerance)
{
	const Eigen::Vector3d d = x.template cast<double>();
	const double dl = d.norm();
	return m_invertSDF*(dl - static_cast<double>(m_radius)) - static_cast<double>(tolerance);
}

bool DistanceFieldCollisionDetection::DistanceFieldCollisionSphere::collisionTest(const Vector3r &x, const Real tolerance, Vector3r &cp, Vector3r &n, Real &dist, const Real maxDist)
{
	const Vector3r d = x;
	const Real dl = d.norm();
	dist = m_invertSDF*(dl - m_radius) - tolerance;
	if (dist < maxDist)
	{
		if (dl < 1.e-6)
			n.setZero();
		else
			n = m_invertSDF * d / dl;

		cp = ((m_radius+tolerance) * n);
		return true;
	}
	return false;
}

double DistanceFieldCollisionDetection::DistanceFieldCollisionTorus::distance(const Eigen::Vector3d &x, const Real tolerance)
{
	const Eigen::Vector2d radii_d = m_radii.template cast<double>();
	const Eigen::Vector2d q(Vector2r(x.x(), x.z()).norm() - radii_d.x(), x.y());
	return m_invertSDF*(q.norm() - radii_d.y()) - tolerance;
}

double DistanceFieldCollisionDetection::DistanceFieldCollisionCylinder::distance(const Eigen::Vector3d &x, const Real tolerance)
{
	const double l = sqrt(x.x()*x.x() + x.z()*x.z());
	const Eigen::Vector2d d = Eigen::Vector2d(fabs(l), fabs(x.y())) - m_dim.template cast<double>();
	const Eigen::Vector2d max_d(std::max(d.x(), 0.0), std::max(d.y(), 0.0));
	return m_invertSDF*(std::min(std::max(d.x(), d.y()), 0.0) + max_d.norm()) - static_cast<double>(tolerance);
}


double DistanceFieldCollisionDetection::DistanceFieldCollisionHollowSphere::distance(const Eigen::Vector3d &x, const Real tolerance)
{
	const Eigen::Vector3d d = x.template cast<double>();
	const double dl = d.norm();
	return m_invertSDF*(fabs(dl - static_cast<double>(m_radius)) - static_cast<double>(m_thickness)) - static_cast<double>(tolerance);
}

bool DistanceFieldCollisionDetection::DistanceFieldCollisionHollowSphere::collisionTest(const Vector3r &x, const Real tolerance, Vector3r &cp, Vector3r &n, Real &dist, const Real maxDist)
{
	const Vector3r d = x;
	const Real dl = d.norm();
	dist = m_invertSDF*(fabs(dl - m_radius) - m_thickness) - tolerance;
	if (dist < maxDist)
	{
		if (dl < 1.e-6)
			n.setZero();
		else if (dl < m_radius)
			n = -m_invertSDF*d / dl;
		else
			n = m_invertSDF*d / dl;

		cp = x - dist * n;
		return true;
	}
	return false;
}

double DistanceFieldCollisionDetection::DistanceFieldCollisionHollowBox::distance(const Eigen::Vector3d &x, const Real tolerance)
{
	const Eigen::Vector3d box_d = m_box.template cast<double>();
	const Eigen::Vector3d x_d = x.template cast<double>();
	const Eigen::Vector3d d = x_d.cwiseAbs() - box_d;
	const Eigen::Vector3d max_d = d.cwiseMax(Eigen::Vector3d(0.0, 0.0, 0.0));
	return m_invertSDF * (fabs(std::min(d.maxCoeff(), 0.0) + max_d.norm()) - m_thickness) - static_cast<double>(tolerance);
}

void DistanceFieldCollisionDetection::DistanceFieldCollisionObject::approximateNormal(const Eigen::Vector3d &x, const Real tolerance, Vector3r &n)
{
	// approximate gradient
	double eps = 1.e-6;
	n.setZero();
	Eigen::Vector3d xTmp = x;
	for (unsigned int j = 0; j < 3; j++)
	{
		xTmp[j] += eps;

		double e_p, e_m;
		e_p = distance(xTmp, tolerance);
		xTmp[j] = x[j] - eps;
		e_m = distance(xTmp, tolerance);
		xTmp[j] = x[j];

		double res = (e_p - e_m) * (1.0 / (2.0*eps));

		n[j] = static_cast<Real>(res);
	}

	const Real norm2 = n.squaredNorm();
	if (norm2 < 1.e-6)
		n.setZero();
	else
		n = n / sqrt(norm2);
}


bool DistanceFieldCollisionDetection::DistanceFieldCollisionObject::collisionTest(const Vector3r &x, const Real tolerance, Vector3r &cp, Vector3r &n, Real &dist, const Real maxDist)
{
	const Real t_d = static_cast<Real>(tolerance);
	dist = static_cast<Real>(distance(x.template cast<double>(), t_d));
	if (dist < maxDist)
	{
		// approximate gradient
		const Eigen::Vector3d x_d = x.template cast<double>();

		approximateNormal(x_d, t_d, n);

		cp = (x - dist * n);
		return true;
	}
	return false;
}

void DistanceFieldCollisionDetection::DistanceFieldCollisionObject::initTetBVH(const Vector3r *vertices, const unsigned int numVertices, const unsigned int *indices, const unsigned int numTets, const Real tolerance)
{
	if (m_bodyType == CollisionDetection::CollisionObject::TetModelCollisionObjectType)
	{
		m_bvhTets.init(vertices, numVertices, indices, numTets, tolerance);
		m_bvhTets.construct();

		// ToDo: copy constructor
		m_bvhTets0.init(vertices, numVertices, indices, numTets, 0.0);
		m_bvhTets0.construct();

	}
}

